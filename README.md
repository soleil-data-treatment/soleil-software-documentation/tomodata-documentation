# Aide et ressources de tomodata pour Synchrotron SOLEIL

## Résumé

- conversion des fichiers entre du nexus vers du edf, paramétrage, reconstruction
- Créé à Synchrotron Soleil

## Sources

- Code source:  https://gitlab.com/soleil-psiche/tomodata
- Documentation officielle:  http://confluence.synchrotron-soleil.fr/display/ANATOMIX/tomodata.py+documentation

## Navigation rapide

| Fichiers téléchargeables |
| - |
| [Documents d'utilisation](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/tomodata-documentation/-/tree/master/documents) |

## Installation

- Systèmes d'exploitation supportés: Linux
- Installation: Difficile (très technique),  pas de setup

## Format de données

- en entrée: nexus,  template fichier par
- en sortie: edf,  fichiers par
- sur un disque dur
